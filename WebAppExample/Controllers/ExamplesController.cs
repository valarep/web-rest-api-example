﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAppExample.Models;

namespace WebAppExample.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExamplesController : ControllerBase
    {
        private readonly ExampleContext _context;

        public ExamplesController(ExampleContext context)
        {
            _context = context;
        }

        // GET: api/examples
        [HttpGet]
        public ActionResult<List<Example>> Get()
        {
            return _context.Get();
        }

        // GET: api/examples/5
        [HttpGet("{id}", Name = "Get")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public ActionResult<Example> Get(int id)
        {
            Example example = _context.Get(id);

            if (example == null)
            {
                return NotFound();
            }

            return example;
        }

        // POST: api/examples
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesDefaultResponseType]
        public ActionResult<Example> Post([FromBody] Example example)
        {
            _context.Add(example);
            return CreatedAtAction(nameof(Post), new { id = example.Id }, example);
        }

        // PUT: api/examples/5
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public IActionResult Put(int id, [FromBody] Example example)
        {
            if(id != example.Id)
            {
                return BadRequest();
            }
            Example e = _context.Get(id);
            if (e == null)
            {
                return NotFound();
            }
            _context.Modify(example);
            return NoContent();
        }

        // DELETE: api/examples/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public IActionResult Delete(int id)
        {
            Example example = _context.Get(id);

            if (example == null)
            {
                return NotFound();
            }

            _context.Remove(example);
            return NoContent();
        }
    }
}
