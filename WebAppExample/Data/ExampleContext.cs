﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace WebAppExample.Models
{
    public class ExampleContext
    {
        public string ConnectionString { get; set; }

        public ExampleContext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }

        public List<Example> Get()
        {
            List<Example> list = new List<Example>();

            using (MySqlConnection connection = GetConnection())
            {
                connection.Open();
                string cmdText = "SELECT * FROM `example`";
                MySqlCommand cmd = new MySqlCommand(cmdText, connection);

                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while(reader.Read())
                    {
                        list.Add(new Example
                        {
                            Id = Convert.ToInt32(reader["id"]),
                            Blob = reader["blob"].ToString(),
                        });
                    }
                }
            }

            return list;
        }

        public Example Get(int id)
        {
            Example item = null;

            using (MySqlConnection connection = GetConnection())
            {
                connection.Open();
                string cmdText = "SELECT * FROM `example` WHERE `id` = ?id";
                MySqlCommand cmd = new MySqlCommand(cmdText, connection);
                cmd.Prepare();
                cmd.Parameters.Add(new MySqlParameter("id", id));

                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        item = new Example
                        {
                            Id = Convert.ToInt32(reader["id"]),
                            Blob = reader["blob"].ToString(),
                        };
                    }
                }
            }

            return item;
        }
        public int Add(Example item)
        {
            int affectedRows = 0;
            using (MySqlConnection connection = GetConnection())
            {
                connection.Open();
                string cmdText = @"INSERT INTO `example`
                                   (`blob`)
                                   VALUES
                                   (?blob)";
                MySqlCommand cmd = new MySqlCommand(cmdText, connection);
                cmd.Prepare();
                cmd.Parameters.Add(new MySqlParameter("blob", item.Blob));

                affectedRows = cmd.ExecuteNonQuery();
                if (affectedRows == 1)
                {
                    item.Id = Convert.ToInt32(cmd.LastInsertedId);
                }
            }

            return affectedRows;
        }
        public int Modify(Example item)
        {
            int affectedRows = 0;
            using (MySqlConnection connection = GetConnection())
            {
                connection.Open();
                string cmdText = @"UPDATE `example`
                                   SET
                                       `blob` = ?blob
                                   WHERE `id` = ?id";
                MySqlCommand cmd = new MySqlCommand(cmdText, connection);
                cmd.Prepare();
                cmd.Parameters.Add(new MySqlParameter("blob", item.Blob));
                cmd.Parameters.Add(new MySqlParameter("id", item.Id));

                affectedRows = cmd.ExecuteNonQuery();
            }

            return affectedRows;
        }
        public int Remove(Example item)
        {
            int affectedRows = 0;
            using (MySqlConnection connection = GetConnection())
            {
                connection.Open();
                string cmdText = @"DELETE FROM `example`
                                   WHERE `id` = ?id";
                MySqlCommand cmd = new MySqlCommand(cmdText, connection);
                cmd.Prepare();
                cmd.Parameters.Add(new MySqlParameter("id", item.Id));

                affectedRows = cmd.ExecuteNonQuery();
            }

            return affectedRows;
        }
    }
}
