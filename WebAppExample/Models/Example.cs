﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppExample.Models
{
    public class Example
    {
        public int Id { get; set; }
        public string Blob { get; set; }
    }
}
